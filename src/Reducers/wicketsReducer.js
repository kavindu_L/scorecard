function wicketsReducer(state = 0, action) {
  switch (action.type) {
    case "setWickets":
      return state + 1;
    case "setWicketsFromLeftBar":
      return action.payload.wickets;
    default:
      return state;
  }
}

export default wicketsReducer;
