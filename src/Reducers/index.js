import scoreReducer from "./scoreReducer";
import wicketsReducer from "./wicketsReducer";
import oversReducer from "./oversReducer";
import ballsReducer from "./ballsReducer";
import { combineReducers } from "redux";

const allReducers = combineReducers({
  score: scoreReducer,
  wickets: wicketsReducer,
  overs: oversReducer,
  balls: ballsReducer
});

export default allReducers;
