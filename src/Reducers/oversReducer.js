function oversReducer(state = 0, action) {
  switch (action.type) {
    case "increaseOvers":
      return state + 1;
    case "decreaseOvers":
      return state - 1;
    default:
      return state;
  }
}

export default oversReducer;
