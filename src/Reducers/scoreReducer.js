function scoreReducer(state = 0, action) {
  switch (action.type) {
    case "increaseScore":
      return state + action.payload.runs;
    case "setScoreFromLeftBar":
      return action.payload.runs;
    default:
      return state;
  }
}

export default scoreReducer;
