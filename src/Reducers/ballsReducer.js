function ballsReducer(state = 0, action) {
  switch (action.type) {
    case "increaseBalls":
      return state + 1;
    case "decreaseBalls":
      return state - 1;
    case "setBalls":
      return action.payload.balls;
    default:
      return state;
  }
}

export default ballsReducer;
