function RunsButtons({ handleScore }) {
  const runs = [0, 1, 2, 3, 4, 5, 6];
  return (
    <div className="m-3">
      <h5>Runs</h5>
      {runs.map(run => (
        <button
          key={run}
          className="btn btn-primary me-2"
          value={run}
          onClick={handleScore}
        >
          {run}
        </button>
      ))}
    </div>
  );
}

export default RunsButtons;
