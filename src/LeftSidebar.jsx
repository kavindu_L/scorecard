import { useDispatch, useSelector } from "react-redux";
import * as scoreActions from "./Actions/scoreActions";
import * as wicketsActions from "./Actions/wicketsActions";
import * as oversActions from "./Actions/oversActions";
import * as ballsActions from "./Actions/ballsActions";
import { useRef } from "react";

function LeftSidebar() {
  const dispatch = useDispatch();

  let changedScoreRef = useRef();
  let changeWicketsRef = useRef();

  const overs = useSelector(state => state.overs);
  const balls = useSelector(state => state.balls);

  function changeScore(event) {
    event.preventDefault();
    dispatch(
      scoreActions.setScoreFromLeftBar(Number(changedScoreRef.current.value))
    );
    changedScoreRef.current.value = "";
  }

  function changeWickets(event) {
    event.preventDefault();

    dispatch(
      wicketsActions.setWicketsFromLeftBar(
        Number(changeWicketsRef.current.value)
      )
    );
    changeWicketsRef.current.value = "";
  }

  function handleOvers(event) {
    let operator = event.target.innerHTML;
    if (operator === "-") {
      dispatch(oversActions.decreaseOvers(overs));
    } else {
      dispatch(oversActions.increaseOvers(overs));
    }
  }

  function changeOvers() {
    if (balls === 5) {
      dispatch(oversActions.increaseOvers(overs));
      dispatch(ballsActions.setBalls(0));
    } else {
      dispatch(ballsActions.increaseBalls());
    }
  }

  function handleBalls(event) {
    let operator = event.target.innerHTML;

    if (operator === "+") {
      changeOvers();
    } else {
      if (balls === 0) {
        dispatch(oversActions.decreaseOvers(overs));
        dispatch(ballsActions.setBalls(5));
      } else {
        dispatch(ballsActions.decreaseBalls());
      }
    }
  }

  return (
    <div>
      <form
        className="input-group input-group-sm mb-3 px-2"
        onSubmit={changeScore}
      >
        <input ref={changedScoreRef} type="number" className="form-control" />
        <button className="btn btn-outline-secondary">Change the score</button>
      </form>

      <form
        className="input-group input-group-sm mb-3 px-2"
        onSubmit={changeWickets}
      >
        <input ref={changeWicketsRef} type="number" className="form-control" />
        <button className="btn btn-outline-secondary">
          Change the wickets
        </button>
      </form>

      <div className="mb-3 px-2">
        <span className="me-3">Change overs</span>
        <button
          className="btn btn-outline-secondary btn-sm mx-2"
          onClick={e => handleOvers(e)}
        >
          -
        </button>
        <span>{overs}</span>
        <button
          className="btn btn-outline-secondary btn-sm mx-2"
          onClick={e => handleOvers(e)}
        >
          +
        </button>
      </div>

      <div className="mb-3 px-2">
        <span className="me-3">Change balls</span>
        <button
          className="btn btn-outline-secondary btn-sm mx-2"
          onClick={e => handleBalls(e)}
        >
          -
        </button>
        <span>{balls}</span>
        <button
          className="btn btn-outline-secondary btn-sm mx-2"
          onClick={e => handleBalls(e)}
        >
          +
        </button>
      </div>
    </div>
  );
}

export default LeftSidebar;
