import React from "react";
import ReactDOM from "react-dom/client";
import Scorecard from "./Scorecard";
import { Provider } from "react-redux";
import { createStore } from "redux";
import allReducers from "./Reducers";

const root = ReactDOM.createRoot(document.getElementById("root"));

const store = createStore(allReducers);

root.render(
  <Provider store={store}>
    <React.StrictMode>
      <Scorecard />
    </React.StrictMode>
  </Provider>
);
