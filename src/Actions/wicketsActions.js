export const setWickets = wickets => {
  return {
    type: "setWickets"
  };
};

export const setWicketsFromLeftBar = wickets => {
  return {
    type: "setWicketsFromLeftBar",
    payload: {
      wickets
    }
  };
};
