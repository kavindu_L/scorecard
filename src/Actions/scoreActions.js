export const increaseScore = runs => {
  return {
    type: "increaseScore",
    payload: {
      runs
    }
  };
};

export const setScoreFromLeftBar = runs => {
  return {
    type: "setScoreFromLeftBar",
    payload: {
      runs
    }
  };
};
