export const increaseBalls = () => {
  return {
    type: "increaseBalls"
  };
};

export const decreaseBalls = () => {
  return {
    type: "decreaseBalls"
  };
};

export const setBalls = balls => {
  return {
    type: "setBalls",
    payload: {
      balls
    }
  };
};
