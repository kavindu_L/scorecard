import LeftSidebar from "./LeftSidebar";
import CenterBar from "./CenterBar";
import RightSidebar from "./RightSidebar";

function Scorecard() {
  return (
    <div className="container mt-3">
      <h2>Scorecard</h2>
      <div className="d-flex justify-content-between">
        <div className="w-25 bg-light rounded align-self-start">
          <LeftSidebar />
        </div>

        <div className="w-50 mx-3 bg-light rounded align-self-start">
          <CenterBar />
        </div>

        <div className="w-25 bg-light rounded align-self-start">
          <RightSidebar />
        </div>
      </div>
    </div>
  );
}

export default Scorecard;
