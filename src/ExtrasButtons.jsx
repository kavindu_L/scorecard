function ExtrasButtons({ extrasType, handleExtras }) {
  let uppaerExtrasButtons;
  let bottomExtrasButtons;
  if (extrasType === "wd" || extrasType === "nb") {
    uppaerExtrasButtons = [1, 2];
    bottomExtrasButtons = [3, 4, 5, 6];
  } else {
    uppaerExtrasButtons = [2, 3];
    bottomExtrasButtons = [4, 5, 6];
  }

  return (
    <div className="d-flex flex-column mb-3" style={{ width: "115px" }}>
      <div className="d-flex justify-content-between">
        <div>
          <button
            className="btn btn-primary btn-lg rounded px-4 pb-4 mb-1"
            value={extrasType}
            onClick={e => handleExtras(e)}
          >
            {extrasType.toUpperCase()}
          </button>
        </div>

        <div>
          {uppaerExtrasButtons.map(ex => (
            <div key={ex}>
              <button
                className="btn btn-outline-primary btn-sm rounded mb-1"
                onClick={e => handleExtras(e)}
                value={extrasType + "+" + ex}
              >
                {ex}
              </button>
            </div>
          ))}
        </div>
      </div>

      <div className="d-flex">
        {bottomExtrasButtons.map(ex => (
          <div key={ex}>
            <button
              className="btn btn-outline-primary btn-sm rounded me-1"
              onClick={e => handleExtras(e)}
              value={extrasType + "+" + ex}
            >
              {ex}
            </button>
          </div>
        ))}
      </div>
    </div>

    // <div className="d-flex  flex-column justify-content-start align-items-center">
    //   {extras.map(ex => (
    //     <button
    //       key={ex}
    //       className="btn btn-primary mx-2 my-1"
    //       value={ex}
    //       onClick={e => handleExtras(e)}
    //     >
    //       {ex === 0 ? extrasType : extrasType + "+" + ex}
    //     </button>
    //   ))}
    // </div>

    // <div
    //   className="btn-group-vertical"
    //   role="group"
    //   aria-label="Vertical button group"
    // >
    //   {extras.map(ex => (
    //     <button
    //       key={ex}
    //       className="btn btn-primary"
    //       value={ex}
    //       onClick={e => handleExtras(e)}
    //     >
    //       {ex === 0 ? extrasType : extrasType + "+" + ex}
    //     </button>
    //   ))}
    // </div>
  );
}

export default ExtrasButtons;
