import { useState, useRef } from "react";
import ExtrasButtons from "./ExtrasButtons";

function Scorecard() {
  const [score, setScore] = useState(0);
  const [wickets, setWickets] = useState(0);
  const [balls, setBalls] = useState(0);
  const [over, setOver] = useState(0);
  const [scoreTimeline, setScoreTimeline] = useState([]);

  let changeTotal = useRef();
  let changeWicketsCount = useRef();

  function handleOvers() {
    if (balls === 5) {
      setOver(currentOver => currentOver + 1);
      setBalls(0);
      setScoreTimeline(currentTimeline => [...currentTimeline, "|"]);
    } else {
      setBalls(currentBalls => currentBalls + 1);
    }
  }

  function handleScore(event) {
    setScore(currentScore => currentScore + Number(event.target.value));

    setScoreTimeline(currentTimeline => [
      ...currentTimeline,
      event.target.value
    ]);

    handleOvers();
  }

  function handleExtras(event) {
    setScore(currentScore => currentScore + Number(event.target.value));

    if (
      event.target.innerHTML.startsWith("b") ||
      event.target.innerHTML.startsWith("lb")
    ) {
      handleOvers();
    }

    setScoreTimeline(currentTimeline => [
      ...currentTimeline,
      event.target.innerHTML
    ]);
  }

  function handleWickets(event) {
    if (wickets < 10) {
      setWickets(currentWickets => currentWickets + Number(event.target.value));
      setScoreTimeline(currentTimeline => [...currentTimeline, "W"]);
      handleOvers();
    }
  }

  function changeScore(event) {
    event.preventDefault();
    // console.log(typeof event.target.score.value);

    setScore(Number(event.target.score.value));
    changeTotal.current.value = "";
  }

  function changeWickets(event) {
    event.preventDefault();
    // console.log(typeof event.target.score.value);

    setWickets(Number(event.target.wicket.value));
    changeWicketsCount.current.value = "";
  }

  function handleBalls(sign) {
    if (sign === "+") {
      handleOvers();
    } else {
      if (balls === 0) {
        setOver(curOver => curOver - 1);
        setBalls(5);
      } else {
        setBalls(curBalls => curBalls - 1);
      }
    }
  }

  return (
    <div className="container mt-3">
      <h2>Scorecard</h2>
      <div className="bg-light rounded text-center p-3">
        <h2>
          {score}/{wickets}
        </h2>
        <span>
          overs {over}.{balls}
        </span>
        <br />
        <div className="opacity-50">
          {scoreTimeline.length > 0 && scoreTimeline.slice(-7).join(" ")}
        </div>
      </div>

      <div className="mt-3">
        <span>runs</span>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="0"
          onClick={handleScore}
        >
          0
        </button>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="1"
          onClick={handleScore}
        >
          1
        </button>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="2"
          onClick={handleScore}
        >
          2
        </button>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="3"
          onClick={handleScore}
        >
          3
        </button>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="4"
          onClick={handleScore}
        >
          4
        </button>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="5"
          onClick={handleScore}
        >
          5
        </button>
        <button
          className="btn btn-primary me-2 rounded-circle"
          value="6"
          onClick={handleScore}
        >
          6
        </button>
      </div>
      <hr />
      <div className="mt-3">
        <span>extras</span>
        <ExtrasButtons extrasType="wide" handleExtras={handleExtras} />
        <ExtrasButtons extrasType="nb" handleExtras={handleExtras} />
        <ExtrasButtons extrasType="b" handleExtras={handleExtras} />
        <ExtrasButtons extrasType="lb" handleExtras={handleExtras} />

        {/* <div>
          <button
            className="btn btn-primary me-2"
            value="1"
            onClick={handleExtras}
          >
            b1
          </button>
          <button
            className="btn btn-primary me-2"
            value="2"
            onClick={handleExtras}
          >
            b2
          </button>
          <button
            className="btn btn-primary me-2"
            value="3"
            onClick={handleExtras}
          >
            b3
          </button>
          <button
            className="btn btn-primary me-2"
            value="4"
            onClick={handleExtras}
          >
            b4
          </button>
          <button
            className="btn btn-primary me-2"
            value="5"
            onClick={handleExtras}
          >
            b5
          </button>
          <button
            className="btn btn-primary me-2"
            value="6"
            onClick={handleExtras}
          >
            b6
          </button>
        </div> */}

        {/* <div>
          <button
            className="btn btn-primary me-2 rounded-circle"
            value="1"
            onClick={handleExtras}
          >
            lb
          </button>
        </div> */}
      </div>
      <hr />

      <div className="mt-3">
        <span>wickets</span>
        <button
          className="btn btn-danger me-2 rounded-circle"
          value="1"
          onClick={handleWickets}
        >
          W
        </button>
      </div>

      <div>
        <form onSubmit={changeScore}>
          <label htmlFor="">score</label>
          <input ref={changeTotal} name="score" type="number" />
          <input type="submit" value="change score" />
        </form>
        <form onSubmit={changeWickets}>
          <label htmlFor="">wickets</label>
          <input ref={changeWicketsCount} name="wicket" type="number" />
          <input type="submit" value="change wickets" />
        </form>
        <div>
          balls
          <button onClick={() => handleBalls("-")}>-</button>
          {balls}
          <button onClick={() => handleBalls("+")}>+</button>
        </div>
        <div>
          overs
          <button onClick={() => setOver(curOver => curOver - 1)}>-</button>
          {over}
          <button onClick={() => setOver(curOver => curOver + 1)}>+</button>
        </div>
      </div>
    </div>
  );
}

export default Scorecard;
