import RunsButtons from "./RunsButtons";
import ExtrasButtons from "./ExtrasButtons";
import * as scoreActions from "./Actions/scoreActions";
import * as oversActions from "./Actions/oversActions";
import * as ballsActions from "./Actions/ballsActions";
import * as wicketsActions from "./Actions/wicketsActions";

import { useDispatch, useSelector } from "react-redux";

function CenterBar() {
  const score = useSelector(state => state.score);
  const wickets = useSelector(state => state.wickets);
  const overs = useSelector(state => state.overs);
  const balls = useSelector(state => state.balls);

  const dispatch = useDispatch();

  function changeOvers() {
    if (balls === 5) {
      dispatch(oversActions.increaseOvers(overs));
      dispatch(ballsActions.setBalls(0));
    } else {
      dispatch(ballsActions.increaseBalls());
    }
  }

  function handleScore(event) {
    dispatch(scoreActions.increaseScore(Number(event.target.value)));
    changeOvers();
  }

  function handleWickets() {
    if (wickets < 10) {
      dispatch(wicketsActions.setWickets());
      changeOvers();
    }
  }

  function handleExtras(event) {
    let extra_type = event.target.value.split("+")[0];
    let extra_runs_taken = event.target.value.split("+")[1];

    if (extra_type === "b" || extra_type === "lb") {
      changeOvers();
    }

    if (extra_type && extra_runs_taken === undefined) {
      return dispatch(scoreActions.increaseScore(1));
    } else {
      if (extra_type === "wd" || extra_type === "nb") {
        return dispatch(
          scoreActions.increaseScore(Number(extra_runs_taken) + 1)
        );
      } else {
        return dispatch(scoreActions.increaseScore(Number(extra_runs_taken)));
      }
    }
  }

  return (
    <div>
      <div className="text-center bg-dark rounded mx-3 py-2 text-white">
        <h1>
          {score}/{wickets}
        </h1>
        <h5>
          Overs {overs}.{balls}
        </h5>
      </div>

      <div>
        <RunsButtons handleScore={handleScore} />

        <div className="ms-3">
          <h5>Wickets</h5>
          <button
            className="btn btn-danger me-2"
            value="1"
            onClick={() => handleWickets()}
          >
            W
          </button>
        </div>

        <h5 className="mt-3 ms-3">Extras</h5>
        <div className="d-flex flex-column">
          <div className="d-flex">
            <div className="m-3 text-center">
              {/* <h5 className="">Wide</h5> */}
              <ExtrasButtons extrasType="wd" handleExtras={handleExtras} />
            </div>
            <div className="m-3 text-center">
              {/* <h5 className="text-center">No ball</h5> */}
              <ExtrasButtons extrasType="nb" handleExtras={handleExtras} />
            </div>
          </div>

          <div className="d-flex">
            <div className="m-3 text-center">
              {/* <h5 className="text-center">Byes</h5> */}
              <ExtrasButtons extrasType="b" handleExtras={handleExtras} />
            </div>

            <div className="m-3 text-center">
              {/* <h5 className="text-center">Leg byes</h5> */}
              <ExtrasButtons extrasType="lb" handleExtras={handleExtras} />
            </div>

            <div className="m-3 text-center">
              {/* <h5 className="">Over throws</h5> */}
              <ExtrasButtons extrasType="ot" handleExtras={handleExtras} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CenterBar;
